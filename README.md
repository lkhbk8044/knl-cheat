# Kingdom New Lands save file editor

KNL-Cheat is a command-line save file editor for the game 'Kingdom: New Lands'.

It is multi-platform (Windows, macOS, Linux) as it is written in Python.

### INSTALL:

First, make sure you have **python3** installed.
Then, use the following commands to download the script and install it.

    wget "https://gitlab.com/mgriseri/knl-cheat/raw/master/knl-cheat.py"
    chmod +x knl-cheat.py
    sudo mv knl-cheat.py /usr/local/bin/knl-cheat

> You can uninstall it by removing the file with `sudo rm /usr/local/bin/knl-cheat`

### USAGE:

Run the script from a terminal window when the game is **NOT** running.

#### Fill wallet:

Player has full wallet:

    knl-cheat --wallet
    
#### Fill bank:

Banker has 10 000 coins:

    knl-cheat --bank

#### Switch mounts:

Commands to switch mount:

    knl-cheat --mount horse
    knl-cheat --mount stag
    knl-cheat --mount unicorn

The argument next to `--mount` must be one of the following:

    horse, warhorse, stag, bear, unicorn, reindeer, spookyhorse

#### God Mode:

Enable god-mode (player is invulnerable):

    knl-cheat --god on

Disable god-mode (player is vulnerable):

    knl-cheat --god off

### NOTE

In case you're wondering, this script **is not** compatible with *Kingdom Two Crowns*.

### LICENSE


GNU GENERAL PUBLIC LICENSE:

[https://www.gnu.org/licenses/gpl.html](https://www.gnu.org/licenses/gpl.html)

### THANKS

Thanks to **EconEuler**, **fiddle_n**, **localcodingmonkey**, **philintheblanks**, **Redzapdos**, **totemcatcher**  from [/r/learnpython](https://www.reddit.com/r/learnpython/) for their feedback and suggestions.

### CONTRIBUTE

If you want to adapt the source code, it is under the GPL, you're free to do so. 

If this script has been helpful to you, feel free to [send a donation](https://www.paypal.me/mgriseri).